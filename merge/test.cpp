#include <testing.h>
#include <util.h>
#include <strict_iterator.h>
#include <merge.h>

namespace tests {

void TestAll() {
    StartTesting();
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
