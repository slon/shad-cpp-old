cmake_minimum_required(VERSION 2.8)
project(reverse-map)

if (TEST_SOLUTION)
  include_directories(../private/reverse-map)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_reverse_map test.cpp)
