cmake_minimum_required(VERSION 2.8)
project(string-view)

if (TEST_SOLUTION)
  include_directories(../private/string-view)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)
endif()

add_executable(test_string_view test.cpp)
