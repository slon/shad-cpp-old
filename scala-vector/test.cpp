#include <catch.hpp>
#include <immutable_vector.h>

#include <string>
#include <vector>
#include <random>

template<typename T>
std::vector<T> get_values(const ImmutableVector<T>& data) {
    std::vector<T> result;
    result.reserve(data.size());
    for (size_t i = 0; i < data.size(); ++i)
        result.push_back(data.get(i));
    return result;
}

std::vector<int> make_range(int count) {
    std::vector<int> result(count);
    for (int i = 0; i < count; ++i)
        result[i] = i;
    return result;
}

TEST_CASE("Constructors", "[vector]") {
    ImmutableVector<int> empty;
    REQUIRE(0u == empty.size());

    ImmutableVector<int> from_list{1, 2, 3};
    REQUIRE(3u == from_list.size());
    std::vector<int> to_check{1, 2, 3};
    REQUIRE(to_check == get_values(from_list));

    std::vector<std::string> origin{"aba", "caba"};
    ImmutableVector<std::string> range(origin.begin(), origin.end());
    REQUIRE(2u == range.size());
    REQUIRE(origin == get_values(range));
}

TEST_CASE("ChangeSize", "[vector]") {
    const int iterations_count = 1000;
    ImmutableVector<int> data;
    std::vector<ImmutableVector<int>> versions;
    versions.reserve(iterations_count);
    for (int i = 0; i < iterations_count; ++i) {
        data = data.push_back(i);
        versions.push_back(data);
        REQUIRE(versions.back().size() == i + 1);
        REQUIRE(make_range(i + 1) == get_values(versions.back()));
    }
    for (int i = 0; i < iterations_count; ++i) {
        data = data.pop_back();
        REQUIRE(data.size() == iterations_count - i - 1);
        REQUIRE(make_range(iterations_count - i - 1) == get_values(data));
    }
}

TEST_CASE("SetGet", "[vector]") {
    const int iterations_count = 1000;
    ImmutableVector<int> data(50);
    std::vector<int> vector_data(50);
    std::mt19937 gen(7346475);
    std::uniform_int_distribution<int> dist(0, data.size() - 1);
    for (int i = 0; i < iterations_count; ++i) {
        int index = dist(gen);
        data = data.set(index, i + 1);
        vector_data[index] = i + 1;
        for (int j = 0; j < 10; ++j) {
            int check_index = dist(gen);
            REQUIRE(vector_data[check_index] == data.get(check_index));
        }
    }
    REQUIRE(vector_data == get_values(data));
}

TEST_CASE("PushBack", "[vector]") {
    ImmutableVector<int> my_data;
    std::vector<int> ok_data;
    for (int i = 0; i < 100; ++i) {
        ok_data.push_back(i);
        my_data = my_data.push_back(i);
        REQUIRE(ok_data == get_values(my_data));
    }
}

TEST_CASE("Mix", "[vector]") {
    int iterations_count = 1000;
    auto fill = make_range(iterations_count);
    ImmutableVector<int> data(fill.begin(), fill.end());
    std::mt19937 gen(7464754);
    for (int j = 0; j < 10; ++j) {
        auto my_data(data);
        std::uniform_int_distribution<int> dist(0, my_data.size() - 1);
        for (int k = 0; k < iterations_count; ++k) {
            int index = dist(gen);
            REQUIRE(index == my_data.get(index));
        }
    }

    for (int i = 0; i < iterations_count; ++i) {
        data = data.pop_back();
    }

    REQUIRE(data.size() == 0u);
}

TEST_CASE("BigTest", "[vector]") {
    const int kIterationsCount = 100000;
    ImmutableVector<int> data;
    std::mt19937 gen(93475);
    std::vector<int> indices(kIterationsCount);
    for (int i = 0; i < kIterationsCount; ++i) {
        data = data.push_back(i);
        indices[i] = i;
    }
    std::shuffle(indices.begin(), indices.end(), gen);
    for (int i = 0; i < kIterationsCount / 2; ++i) {
        data = data.set(indices[i], -1);
    }

    for (size_t i = 0; i < data.size(); ++i) {
        int cur_value = data.get(indices[i]);
        if (i < kIterationsCount / 2) {
            REQUIRE(cur_value == -1);
        } else {
            REQUIRE(cur_value == indices[i]);
        }
    }
}
